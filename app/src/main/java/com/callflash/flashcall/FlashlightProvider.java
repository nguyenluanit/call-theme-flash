package com.callflash.flashcall;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import androidx.annotation.RequiresApi;

public class FlashlightProvider {

    private static final String TAG = FlashlightProvider.class.getSimpleName();
    private Camera mCamera;
    private Camera.Parameters parameters;
    private CameraManager camManager;
    private Context context;
    private HandlerThread handlerThread;
    private int currentFlashInOneFlash = 0;

    public FlashlightProvider(Context context) {
        this.context = context;
    }

    public void turnFlashlightOn() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                camManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
                String cameraId = null;
                if (camManager != null) {
                    cameraId = camManager.getCameraIdList()[0];
                    camManager.setTorchMode(cameraId, true);
                }
            } catch (CameraAccessException e) {
                Log.e(TAG, e.toString());
            }
        } else {
            mCamera = Camera.open();
            parameters = mCamera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            mCamera.setParameters(parameters);
            mCamera.startPreview();
        }
    }

    public void startThread() {
        handlerThread = new HandlerThread("background-thread");
        handlerThread.start();
    }

    public void toggleFlash(final int totalNumber, final int numberForOneFlash, final int durationForOneFlash, final int duration) {
        if (numberForOneFlash == 0) {
            turnFlashlightOn();
            return;
        }

        if (totalNumber != 0) {
            if (handlerThread != null && handlerThread.isAlive()) {
                toggleFlashOneFlash(numberForOneFlash, durationForOneFlash);
                final Handler handler = new Handler(handlerThread.getLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (currentFlashInOneFlash == 0) {
                            currentFlashInOneFlash = numberForOneFlash;
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    toggleFlash(totalNumber - 1, numberForOneFlash, durationForOneFlash, duration);
                                }
                            }, duration);
                        }
                    }
                }, duration);
            }
        } else {
            handlerThread.quitSafely();
            turnFlashlightOff();
        }
    }

    public void cancelFlash() {
        if (handlerThread != null){
            handlerThread.quitSafely();
            turnFlashlightOff();
        }
    }

    public void toggleFlashOneFlash(final int numberForOneFlash, final int durationForOneFlash) {
        if (numberForOneFlash != 0) {
            currentFlashInOneFlash = numberForOneFlash;
            if (handlerThread != null && handlerThread.isAlive()) {
                turnFlashlightOn();
                final Handler handler = new Handler(handlerThread.getLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        turnFlashlightOff();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                toggleFlashOneFlash(numberForOneFlash - 1, durationForOneFlash);
                            }
                        }, durationForOneFlash);
                    }
                }, durationForOneFlash);
            }
        } else currentFlashInOneFlash = 0;
    }

    public void turnFlashlightOff() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                String cameraId;
                camManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
                if (camManager != null) {
                    cameraId = camManager.getCameraIdList()[0]; // Usually front camera is at 0 position.
                    camManager.setTorchMode(cameraId, false);
                }
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        } else {
            mCamera = Camera.open();
            parameters = mCamera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(parameters);
            mCamera.stopPreview();
        }
    }
}