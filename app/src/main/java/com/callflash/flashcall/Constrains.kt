package com.callflash.flashcall

class Constrains {
    companion object{
        const val isReceiverInComingCall = "isReceiverInComingCall"
        const val isTurnOnLedFlash = "isTurnOnLedFlash"
        const val nameGifSelected = "gifUrlSelected"

        const val RESULT_PREVIEW = 1001
        const val COUNT_FLASH = "COUNT_FLASH"
    }
}