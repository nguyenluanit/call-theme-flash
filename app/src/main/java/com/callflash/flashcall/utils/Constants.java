package com.callflash.flashcall.utils;

public final class Constants {

    public static final String ACTION_DECLINE = "com.bravo.dialer.dialer.ACTION_DECLINE";
    public static final String ACTION_ANSWER = "com.bravo.dialer.dialer.ACTION_ANSWER";

    public static String asString(int data) {
        String value;
        switch (data) {
            case 0:
                value = "New";
                break;
            case 1:
                value = "Dialing";
                break;
            case 2:
                value = "Ringing";
                break;
            case 3:
                value = "Holding";
                break;
            case 4:
                value = "Active";
                break;
            case 7:
                value = "Disconnected";
                break;
            case 8:
                value = "SELECT_PHONE_ACCOUNT";
                break;
            case 9:
                value = "Connecting";
                break;
            case 10:
                value = "Disconnecting";
                break;
            default:
                value = "Unknown";
                break;
        }
        return value;
    }

}
