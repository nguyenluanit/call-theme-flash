package com.callflash.flashcall.utils

import android.animation.Animator
import android.widget.ImageView

class Animation {
    companion object{
        fun startAnimation(imageViewAnswerCall: ImageView) {
            imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {}

                override fun onAnimationEnd(animation: Animator?) {
                    imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator?) {}

                        override fun onAnimationEnd(animation: Animator?) {
                            bubbleAnimation(imageViewAnswerCall)
                        }

                        override fun onAnimationCancel(animation: Animator?) {}

                        override fun onAnimationStart(animation: Animator?) {}

                    }).translationY(0f).setDuration(200).start()
                }

                override fun onAnimationCancel(animation: Animator?) {}

                override fun onAnimationStart(animation: Animator?) {}

            }).translationY(-100f).setDuration(500).start()
        }

        private fun bubbleAnimation(imageViewAnswerCall: ImageView) {
            imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {}

                override fun onAnimationEnd(animation: Animator?) {
                    imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator?) {}

                        override fun onAnimationEnd(animation: Animator?) {
                            imageViewAnswerCall.animate()
                                .setListener(object : Animator.AnimatorListener {
                                    override fun onAnimationRepeat(animation: Animator?) {}

                                    override fun onAnimationEnd(animation: Animator?) {
                                        imageViewAnswerCall.animate()
                                            .setListener(object : Animator.AnimatorListener {
                                                override fun onAnimationRepeat(animation: Animator?) {}

                                                override fun onAnimationEnd(animation: Animator?) {
                                                    startAnimation(imageViewAnswerCall)
                                                }

                                                override fun onAnimationCancel(animation: Animator?) {}

                                                override fun onAnimationStart(animation: Animator?) {}

                                            }).translationY(0f).setDuration(200).start()
                                    }

                                    override fun onAnimationCancel(animation: Animator?) {}

                                    override fun onAnimationStart(animation: Animator?) {}

                                }).translationY(-25f).setDuration(200).start()
                        }

                        override fun onAnimationCancel(animation: Animator?) {}

                        override fun onAnimationStart(animation: Animator?) {}

                    }).translationY(0f).setDuration(200).start()
                }

                override fun onAnimationCancel(animation: Animator?) {}

                override fun onAnimationStart(animation: Animator?) {}

            }).translationY(-25f).setDuration(200).start()
        }
    }
}