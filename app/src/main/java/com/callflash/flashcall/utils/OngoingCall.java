package com.callflash.flashcall.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Build;
import android.telecom.Call;
import android.telecom.VideoProfile;

import androidx.annotation.RequiresApi;

import org.jetbrains.annotations.Nullable;

import io.reactivex.subjects.BehaviorSubject;

public class OngoingCall {

    public static BehaviorSubject<Integer> state = BehaviorSubject.create();
    private static Call sCall;

    public Call getsCall() {
        return sCall;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private Object callback = new Call.Callback() {
        @Override
        public void onStateChanged(Call call, int newState) {
            super.onStateChanged(call, newState);
            state.onNext(newState);
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    public final void truecall_phone_setCall(@Nullable Call value) {
        if (sCall != null) {
            sCall.unregisterCallback((Call.Callback) callback);
        }

        if (value != null) {
            value.registerCallback((Call.Callback) callback);
            state.onNext(value.getState());
        }

        sCall = value;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void truecall_phone_answer() {
        if (sCall != null) {
            assert sCall != null;
            sCall.answer(VideoProfile.STATE_AUDIO_ONLY);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void truecall_phone_hold(boolean hold) {
        if (sCall != null) {
            if (hold) sCall.hold();
            else sCall.unhold();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void truecall_phone_addCall(Call call) {
        if (sCall != null) {
            sCall.conference(call);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void truecall_phone_hangup() {
        if (sCall != null) {
            assert sCall != null;
            sCall.disconnect();
        }
    }

    //add junk code
    public static void mapPoints(Matrix matrix, float[] points) {
        float[] m = {0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F};
        matrix.getValues(m);

        points[0] = (points[0] * m[0] + m[2]);
        points[1] = (points[1] * m[4] + m[5]);

        if (points.length == 4) {
            points[2] = (points[2] * m[0] + m[2]);
            points[3] = (points[3] * m[4] + m[5]);
        }
    }

    private void overlay(Bitmap bm1, Bitmap bm2) {
        // Combine bitmaps
        Bitmap bmOverlay = Bitmap.createBitmap(bm1.getWidth(), bm1.getHeight(), bm1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        Paint paint = new Paint();
        paint.setAlpha(128);
        canvas.drawBitmap(bm1, new Matrix(), null);
        canvas.drawBitmap(bm2, 0, 0, paint);
    }
}
