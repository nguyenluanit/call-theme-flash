package com.callflash.flashcall.features.settings

import android.Manifest
import android.app.Dialog
import android.app.role.RoleManager
import android.content.Context.TELECOM_SERVICE
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.Settings
import android.telecom.TelecomManager
import android.view.*
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.callflash.flashcall.Constrains
import com.callflash.flashcall.R
import kotlinx.android.synthetic.main.fragment_settings.*

/**
 * A simple [Fragment] subclass.
 */
class SettingsFragment : Fragment() {

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var dialog: Dialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        switchLedFlash.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (requireContext().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), 2002)
                } else {
                    val newChecked = switchLedFlash.isChecked
                    sharedPreferences.edit().putBoolean(Constrains.isTurnOnLedFlash, newChecked)
                        .apply()
                }
            } else {
                val newChecked = switchLedFlash.isChecked
                sharedPreferences.edit().putBoolean(Constrains.isTurnOnLedFlash, newChecked)
                    .apply()
            }
        }
        relativeLayoutShare.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            val shareBody = "Download my app"
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_SUBJECT, "Share app")
            intent.putExtra(Intent.EXTRA_TEXT, shareBody)
            startActivity(Intent.createChooser(intent, "Choose with"))
        }
        relativeLayoutPolicy.setOnClickListener {
            val url = "http://www.example.com"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
        relativeLayoutPermissionGuide.setOnClickListener {
            showDialogPermission()
        }
    }

    private fun showDialogPermission() {
        dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_permission_using)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                Color.TRANSPARENT
            )
        )
        val lq = WindowManager.LayoutParams()
        lq.copyFrom(dialog.window?.attributes)
        lq.width = WindowManager.LayoutParams.WRAP_CONTENT
        lq.height = WindowManager.LayoutParams.WRAP_CONTENT
        lq.gravity = Gravity.CENTER
        dialog.window?.attributes = lq
        val relativeDismissDialog =
            dialog.findViewById<RelativeLayout>(R.id.relativeDismissDialog)!!
        relativeDismissDialog.setOnClickListener { dialog.dismiss() }
        val linearLayoutPhoneAndContact =
            dialog.findViewById<LinearLayout>(R.id.linearLayoutPhoneAndContact)!!
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                requireContext().checkSelfPermission(Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED
            ) {
                linearLayoutPhoneAndContact.setBackgroundResource(R.drawable.bg_permission_unable)
                val textViewPhoneAndContact =
                    dialog.findViewById<TextView>(R.id.textViewPhoneAndContact)!!
                textViewPhoneAndContact.setTextColor(resources.getColor(R.color.colorGreen))
                val textViewDetailsPhoneAndContact =
                    dialog.findViewById<TextView>(R.id.textViewDetailsPhoneAndContact)!!
                textViewDetailsPhoneAndContact.setTextColor(resources.getColor(R.color.colorGreen))
            }
        }

        linearLayoutPhoneAndContact.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (requireContext().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                    requireContext().checkSelfPermission(Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.READ_CONTACTS,
                            Manifest.permission.WRITE_CONTACTS
                        ), 5005
                    )
                }
            }
        }
        val linearLayoutStorage = dialog.findViewById<LinearLayout>(R.id.linearLayoutStorage)!!
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                requireContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            ) {
                linearLayoutStorage.setBackgroundResource(R.drawable.bg_permission_unable)
                val textViewStorage =
                    dialog.findViewById<TextView>(R.id.textViewStorage)!!
                textViewStorage.setTextColor(resources.getColor(R.color.colorGreen))
                val textViewDetailsStorage =
                    dialog.findViewById<TextView>(R.id.textViewDetailsStorage)!!
                textViewDetailsStorage.setTextColor(resources.getColor(R.color.colorGreen))
            }
        }
        linearLayoutStorage.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (requireContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    requireContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 6006
                    )
                }
            }
        }
        val linearLayoutCallPermission =
            dialog.findViewById<LinearLayout>(R.id.linearLayoutCallPermission)!!
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!isAlreadyDefaultDialer()) {
                linearLayoutCallPermission.setBackgroundResource(R.drawable.bg_permission_unable)
                val textViewCallPermission =
                    dialog.findViewById<TextView>(R.id.textViewCallPermission)!!
                textViewCallPermission.setTextColor(resources.getColor(R.color.colorGreen))
                val textViewDetailsCallPermission =
                    dialog.findViewById<TextView>(R.id.textViewDetailsCallPermission)!!
                textViewDetailsCallPermission.setTextColor(resources.getColor(R.color.colorGreen))
            }
        }
        linearLayoutCallPermission.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!isAlreadyDefaultDialer()) {
                    checkDefaultApp()
                }else{
                    linearLayoutCallPermission.setBackgroundResource(R.drawable.bg_rl_applied)
                    val textViewCallPermission =
                        dialog.findViewById<TextView>(R.id.textViewCallPermission)!!
                    textViewCallPermission.setTextColor(resources.getColor(R.color.colorWhite))
                    val textViewDetailsCallPermission =
                        dialog.findViewById<TextView>(R.id.textViewDetailsCallPermission)!!
                    textViewDetailsCallPermission.setTextColor(resources.getColor(R.color.colorWhite))
                }
            }
        }
        dialog.show()
    }

    private fun checkDefaultApp() {
        val intent: Intent?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.MODEL == "Vivo") {
                Toast.makeText(
                    requireContext(),
                    "Go to Settings -> More settings -> Permission management(Applications) -> Default app settings to change the default apps.",
                    Toast.LENGTH_SHORT
                ).show()
                startActivityForResult(
                    Intent(Settings.ACTION_SETTINGS),
                    0
                )
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    val roleManager: RoleManager? =
                        requireActivity().getSystemService(RoleManager::class.java)
                    if (roleManager != null) {
                        if (roleManager.isRoleAvailable(RoleManager.ROLE_DIALER)) {
                            if (!roleManager.isRoleHeld(RoleManager.ROLE_DIALER)) {
                                val roleRequestIntent =
                                    roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER)
                                startActivityForResult(
                                    roleRequestIntent,
                                    3321
                                )
                            }
                        }
                    }
                } else {
                    intent =
                        Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER)
                            .putExtra(
                                TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME,
                                requireActivity().packageName
                            )
                    if (intent.resolveActivity(requireActivity().packageManager) != null) {
                        startActivityForResult(
                            intent,
                            3321
                        )
                    } else {
                        throw RuntimeException("Default phone functionality not found")
                    }
                }
            }
        }
    }

    private fun isAlreadyDefaultDialer(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val roleManager: RoleManager? =
                requireActivity().getSystemService(RoleManager::class.java)
            if (roleManager != null) {
                if (roleManager.isRoleAvailable(RoleManager.ROLE_DIALER)) {
                    return roleManager.isRoleHeld(RoleManager.ROLE_DIALER)
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val telecomManager =
                requireActivity().getSystemService(TELECOM_SERVICE) as TelecomManager
            return requireActivity().packageName == telecomManager.defaultDialerPackage
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 2002) {
            switchLedFlash.isChecked = grantResults[0] == PackageManager.PERMISSION_GRANTED
        } else if (requestCode == 5005) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (::dialog.isInitialized && dialog.isShowing) {
                    val linearLayoutPhoneAndContact =
                        dialog.findViewById<LinearLayout>(R.id.linearLayoutPhoneAndContact)!!
                    linearLayoutPhoneAndContact.setBackgroundResource(R.drawable.bg_rl_applied)
                    val textViewPhoneAndContact =
                        dialog.findViewById<TextView>(R.id.textViewPhoneAndContact)!!
                    textViewPhoneAndContact.setTextColor(resources.getColor(R.color.colorWhite))
                    val textViewDetailsPhoneAndContact =
                        dialog.findViewById<TextView>(R.id.textViewDetailsPhoneAndContact)!!
                    textViewDetailsPhoneAndContact.setTextColor(resources.getColor(R.color.colorWhite))
                }
            }
        } else if (requestCode == 6006) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (::dialog.isInitialized && dialog.isShowing) {
                    val linearLayoutStorage =
                        dialog.findViewById<LinearLayout>(R.id.linearLayoutStorage)!!
                    linearLayoutStorage.setBackgroundResource(R.drawable.bg_rl_applied)
                    val textViewStorage =
                        dialog.findViewById<TextView>(R.id.textViewStorage)!!
                    textViewStorage.setTextColor(resources.getColor(R.color.colorWhite))
                    val textViewDetailsStorage =
                        dialog.findViewById<TextView>(R.id.textViewDetailsStorage)!!
                    textViewDetailsStorage.setTextColor(resources.getColor(R.color.colorWhite))
                }
            }
        } else if (requestCode == 3321) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (::dialog.isInitialized && dialog.isShowing) {
                    val linearLayoutCallPermission =
                        dialog.findViewById<LinearLayout>(R.id.linearLayoutCallPermission)!!
                    linearLayoutCallPermission.setBackgroundResource(R.drawable.bg_rl_applied)
                    val textViewCallPermission =
                        dialog.findViewById<TextView>(R.id.textViewCallPermission)!!
                    textViewCallPermission.setTextColor(resources.getColor(R.color.colorWhite))
                    val textViewDetailsCallPermission =
                        dialog.findViewById<TextView>(R.id.textViewDetailsCallPermission)!!
                    textViewDetailsCallPermission.setTextColor(resources.getColor(R.color.colorWhite))
                }
            }
        }
    }

}
