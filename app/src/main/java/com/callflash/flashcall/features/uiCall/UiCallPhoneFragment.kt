package com.callflash.flashcall.features.uiCall

import android.Manifest
import android.app.Activity
import android.app.role.RoleManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.telecom.TelecomManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.callflash.flashcall.Constrains
import com.callflash.flashcall.R
import com.callflash.flashcall.adapters.UiCallAdapter
import com.callflash.flashcall.models.Theme
import kotlinx.android.synthetic.main.fragment_ui_call_phone.*

/**
 * A simple [Fragment] subclass.
 */
class UiCallPhoneFragment : Fragment() {

    private val listTheme = arrayListOf<Theme>()
    private lateinit var adapter: UiCallAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ui_call_phone, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listTheme.clear()
        listTheme.add(
            Theme(
                "1",
                "https://i.imgur.com/3QL5Qnq.jpg",
                "https://i.imgur.com/71isVzw.mp4"
            )
        )
        listTheme.add(
            Theme(
                "2",
                "https://i.imgur.com/2hYzLMp.jpg",
                "https://i.imgur.com/HIo9Uu6.mp4"
            )
        )
        listTheme.add(
            Theme(
                "3",
                "https://i.imgur.com/caZLwwi.jpg",
                "https://i.imgur.com/a7JzTib.mp4"
            )
        )
        listTheme.add(
            Theme(
                "4",
                "https://i.imgur.com/JLoT36u.jpg",
                "https://i.imgur.com/SMKKUjh.mp4"
            )
        )
        listTheme.add(
            Theme(
                "5",
                "https://i.imgur.com/UDy2E1B.jpg",
                "https://i.imgur.com/n3iX11q.gif"
            )
        )
        listTheme.add(
            Theme(
                "6",
                "https://i.imgur.com/7aMEwyT.jpg",
                "https://i.imgur.com/Gyx5YEN.mp4"
            )
        )
        adapter = UiCallAdapter(
            requireContext(),
            this,
            listTheme
        )
        recyclerView.adapter = adapter


        checkDefaultApp()
    }

    private fun checkDefaultApp() {
        val intent: Intent?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.MODEL == "Vivo") {
                Toast.makeText(
                    requireContext(),
                    "Go to Settings -> More settings -> Permission management(Applications) -> Default app settings to change the default apps.",
                    Toast.LENGTH_SHORT
                ).show()
                startActivityForResult(
                    Intent(Settings.ACTION_SETTINGS),
                    0
                )
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    val roleManager: RoleManager? =
                        requireActivity().getSystemService(RoleManager::class.java)
                    if (roleManager != null) {
                        if (roleManager.isRoleAvailable(RoleManager.ROLE_DIALER)) {
                            if (!roleManager.isRoleHeld(RoleManager.ROLE_DIALER)) {
                                val roleRequestIntent =
                                    roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER)
                                startActivityForResult(
                                    roleRequestIntent,
                                    123
                                )
                            } else {
                                if (!checkAllPermission()) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(
                                            arrayOf(
                                                Manifest.permission.READ_PHONE_STATE,
                                                Manifest.permission.PROCESS_OUTGOING_CALLS,
                                                Manifest.permission.READ_CONTACTS,
                                                Manifest.permission.CALL_PHONE,
                                                Manifest.permission.READ_CALL_LOG,
                                                Manifest.permission.WRITE_CONTACTS,
                                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                                Manifest.permission.ANSWER_PHONE_CALLS
                                            ), 3003
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (isAlreadyDefaultDialer()){
                if (!checkAllPermission()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.PROCESS_OUTGOING_CALLS,
                                Manifest.permission.READ_CONTACTS,
                                Manifest.permission.CALL_PHONE,
                                Manifest.permission.READ_CALL_LOG,
                                Manifest.permission.WRITE_CONTACTS,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.ANSWER_PHONE_CALLS
                            ), 3003
                        )
                    }
                }
            }else{
                checkDefaultAppDialer()
            }
        }
    }

    private fun checkDefaultAppDialer() {
        val intent: Intent?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.MODEL == "Vivo") {
                Toast.makeText(
                    requireContext(),
                    "Go to Settings -> More settings -> Permission management(Applications) -> Default app settings to change the default apps.",
                    Toast.LENGTH_SHORT
                ).show()
                startActivityForResult(
                    Intent(Settings.ACTION_SETTINGS),
                    0
                )
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    val roleManager: RoleManager? =
                        requireActivity().getSystemService(RoleManager::class.java)
                    if (roleManager != null) {
                        if (roleManager.isRoleAvailable(RoleManager.ROLE_DIALER)) {
                            if (!roleManager.isRoleHeld(RoleManager.ROLE_DIALER)) {
                                val roleRequestIntent =
                                    roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER)
                                startActivityForResult(
                                    roleRequestIntent,
                                    3321
                                )
                            }
                        }
                    }
                } else {
                    intent =
                        Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER)
                            .putExtra(
                                TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME,
                                requireActivity().packageName
                            )
                    if (intent.resolveActivity(requireActivity().packageManager) != null) {
                        startActivityForResult(
                            intent,
                            3321
                        )
                    } else {
                        throw RuntimeException("Default phone functionality not found")
                    }
                }
            }
        }
    }

    private fun isAlreadyDefaultDialer(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val roleManager: RoleManager? =
                requireActivity().getSystemService(RoleManager::class.java)
            if (roleManager != null) {
                if (roleManager.isRoleAvailable(RoleManager.ROLE_DIALER)) {
                    return roleManager.isRoleHeld(RoleManager.ROLE_DIALER)
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val telecomManager =
                requireActivity().getSystemService(Context.TELECOM_SERVICE) as TelecomManager
            return requireActivity().packageName == telecomManager.defaultDialerPackage
        }
        return true
    }

    private fun checkAllPermission(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requireContext().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED &&
                    requireContext().checkSelfPermission(Manifest.permission.PROCESS_OUTGOING_CALLS) == PackageManager.PERMISSION_GRANTED &&
                    requireContext().checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                    requireContext().checkSelfPermission(Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                    requireContext().checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED &&
                    requireContext().checkSelfPermission(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED &&
                    requireContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    requireContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    requireContext().checkSelfPermission(Manifest.permission.ANSWER_PHONE_CALLS) == PackageManager.PERMISSION_GRANTED

        } else true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constrains.RESULT_PREVIEW) {
            if (resultCode == Activity.RESULT_OK) {
                adapter.notifyDataSetChanged()
            }
        } else if (requestCode == 123) {
            if (resultCode == Activity.RESULT_OK) {
                if (!checkAllPermission()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.PROCESS_OUTGOING_CALLS,
                                Manifest.permission.READ_CONTACTS,
                                Manifest.permission.CALL_PHONE,
                                Manifest.permission.READ_CALL_LOG,
                                Manifest.permission.WRITE_CONTACTS,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.ANSWER_PHONE_CALLS
                            ), 3003
                        )
                    }
                }
            }
        }
    }
}