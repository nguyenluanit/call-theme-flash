package com.callflash.flashcall.features.flash

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.callflash.flashcall.Constrains
import com.callflash.flashcall.FlashlightProvider
import com.callflash.flashcall.R
import com.callflash.flashcall.adapters.FlashAdapter
import com.callflash.flashcall.commons.MyLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_flash_light.*
import kotlin.math.max
import kotlin.math.min


/**
 * A simple [Fragment] subclass.
 */
class FlashLightFragment : Fragment() {

    private lateinit var flashlightProvider: FlashlightProvider
    private var isTurnOn = false
    private var currentPosition = 0
    private lateinit var adapter: FlashAdapter
    private lateinit var myLinearLayoutManager: MyLinearLayoutManager
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        flashlightProvider = FlashlightProvider(requireContext())
        return inflater.inflate(R.layout.fragment_flash_light, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        listenerView()
    }

    private fun initRecyclerView() {
        val snapHelper: LinearSnapHelper = object : LinearSnapHelper() {
            override fun findTargetSnapPosition(
                layoutManager: RecyclerView.LayoutManager,
                velocityX: Int,
                velocityY: Int
            ): Int {
                val centerView = findSnapView(layoutManager) ?: return RecyclerView.NO_POSITION
                val position = layoutManager.getPosition(centerView)
                var targetPosition = -1
                if (layoutManager.canScrollHorizontally()) {
                    targetPosition = if (velocityX < 0) {
                        position - 1
                    } else {
                        position + 1
                    }
                }
                if (layoutManager.canScrollVertically()) {
                    targetPosition = if (velocityY < 0) {
                        position - 1
                    } else {
                        position + 1
                    }
                }
                val firstItem = 0
                val lastItem = layoutManager.itemCount - 1
                targetPosition =
                    min(lastItem, max(targetPosition, firstItem))
                return targetPosition
            }
        }
        myLinearLayoutManager =
            MyLinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        adapter = FlashAdapter(requireContext())
        recyclerViewFlash.layoutManager = myLinearLayoutManager
        recyclerViewFlash.adapter = adapter
        recyclerViewFlash.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val centerView: View = snapHelper.findSnapView(myLinearLayoutManager)!!
                    currentPosition = myLinearLayoutManager.getPosition(centerView)
                    adapter.setCurrentView(
                        position = currentPosition
                    )
                    sharedPreferences.edit().putInt(Constrains.COUNT_FLASH, currentPosition % 10)
                        .apply()
                }
            }
        })
        snapHelper.attachToRecyclerView(recyclerViewFlash)
        recyclerViewFlash.smoothScrollToPosition(50)
    }

    private fun listenerView() {
        imageViewToggleFlash.setOnClickListener {
            if (isTurnOn) {
                myLinearLayoutManager.setCanScrollHorizon(true)
                imageViewToggleFlash.setImageResource(R.drawable.ic_flash_off)
                flashlightProvider.cancelFlash()
            } else {
                myLinearLayoutManager.setCanScrollHorizon(false)
                imageViewToggleFlash.setImageResource(R.drawable.ic_flash_on)
                flashlightProvider.startThread()
                flashlightProvider.toggleFlash(
                    1000,
                    currentPosition % 10,
                    100,
                    ((currentPosition % 10) * 100) * 3
                )
            }
            isTurnOn = !isTurnOn
        }
    }

}