package com.callflash.flashcall.features.phone

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import com.callflash.flashcall.Constrains
import com.callflash.flashcall.activitys.ReceiveIncomingCallActivity
import java.util.*

class CallReceiver : PhonecallReceiver() {
    override fun onIncomingCallReceived(
        ctx: Context,
        number: String?
    ) {
//        if (number != null && number.isNotEmpty()) {
//            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
//            if (sharedPreferences.getBoolean(Constrains.isReceiverInComingCall, false)) {
//                Handler().postDelayed({
//                    val intent = Intent(ctx, ReceiveIncomingCallActivity::class.java)
//                    intent.putExtra("numberPhone", number)
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                    ctx.startActivity(intent)
//                }, 1000)
//            }
//        }
    }

    override fun onIncomingCallAnswered(
        ctx: Context,
        number: String,
        start: Date
    ) {
    }

    override fun onIncomingCallEnded(
        ctx: Context,
        number: String,
        start: Date,
        end: Date
    ) {
    }

    override fun onOutgoingCallStarted(
        ctx: Context,
        number: String,
        start: Date
    ) {
    }

    override fun onOutgoingCallEnded(
        ctx: Context,
        number: String,
        start: Date,
        end: Date
    ) {
    }

    override fun onMissedCall(
        ctx: Context,
        number: String,
        start: Date
    ) {
    }
}