package com.callflash.flashcall.activitys

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.callflash.flashcall.R
import com.callflash.flashcall.commons.MyAppActivity
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : MyAppActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setViewTermsAndPolicy()
        Handler().postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }, 3000)
    }

    private fun setViewTermsAndPolicy() {
        val spanString = SpannableString(
            "By using the functions of the app, you are agreeing to Call Flash's \n Temps of Service and Privacy Policy"
        )
        spanString.setSpan(UnderlineSpan(), 70, 86, 0)
        spanString.setSpan(UnderlineSpan(), 91, spanString.length, 0)
        val termsClickListener = object : ClickableSpan() {
            override fun onClick(widget: View) {
                Toast.makeText(this@SplashActivity, "Terms", Toast.LENGTH_SHORT).show()
            }
        }
        val policyClickListener = object : ClickableSpan() {
            override fun onClick(widget: View) {
                Toast.makeText(this@SplashActivity, "Policy", Toast.LENGTH_SHORT).show()
            }
        }
        spanString.setSpan(termsClickListener, 70, 86, 0)
        spanString.setSpan(policyClickListener, 91, spanString.length, 0)
        textViewTempsAndPolicy.movementMethod = LinkMovementMethod.getInstance()
        textViewTempsAndPolicy.setText(spanString, TextView.BufferType.SPANNABLE)
        textViewTempsAndPolicy.isSelected = true
    }

}