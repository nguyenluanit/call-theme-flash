package com.callflash.flashcall.activitys

import android.Manifest
import android.animation.Animator
import android.app.Activity
import android.app.KeyguardManager
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.provider.ContactsContract
import android.telephony.TelephonyManager
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.core.net.toUri
import com.callflash.flashcall.Constrains
import com.callflash.flashcall.DownloadFileFromURL
import com.callflash.flashcall.FlashlightProvider
import com.callflash.flashcall.R
import com.callflash.flashcall.commons.MyAppActivity
import com.android.internal.telephony.ITelephony
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_preview_ui_call.*
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.lang.reflect.Method


class ReceiveIncomingCallActivity : MyAppActivity() {

    private val MANUFACTURER_HTC = "HTC"
    private var isPreview = false
    private lateinit var keyguardManager: KeyguardManager
    private lateinit var audioManager: AudioManager
    private var callStateReceiver: CallStateReceiver? = null
    private lateinit var flashlightProvider: FlashlightProvider

    private lateinit var sharedPreferences: SharedPreferences

    private var name: String = ""
    private var gifUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview_ui_call)

        isPreview = intent.getBooleanExtra("isPreview", false)
        if (isPreview) {
            initData()
            listenerApplyUi()
        } else {
            keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            flashlightProvider = FlashlightProvider(this)
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            val countFlash = sharedPreferences.getInt(Constrains.COUNT_FLASH, 5)
            flashlightProvider.startThread()
            flashlightProvider.toggleFlash(
                1000,
                countFlash % 10,
                100,
                ((countFlash % 10) * 100) * 3
            )

            setDataPhone()
            listenerCall()
        }
        startAnimation()
    }

    override fun onResume() {
        super.onResume()

        if (!isPreview) {
            registerCallStateReceiver()
            updateWindowFlags()
        }
    }

    override fun onPause() {
        super.onPause()
        if (!isPreview) {
            if (callStateReceiver != null) {
                unregisterReceiver(callStateReceiver)
                callStateReceiver = null
            }
        }
    }

    private fun updateWindowFlags() {
        if (keyguardManager.inKeyguardRestrictedInputMode()) {
            window.addFlags(
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON or
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
            )
        } else {
            window.clearFlags(
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
            )
        }
    }

    private fun disconnectPhoneItelephony(context: Context) {
        val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        try {
            val c = Class.forName(tm.javaClass.name)
            val m: Method = c.getDeclaredMethod("getITelephony")
            m.isAccessible = true
            val telephonyService = m.invoke(tm) as ITelephony
            telephonyService.silenceRinger()
            telephonyService.endCall()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun listenerCall() {
        imageViewDeclineCall.setOnClickListener {
            disconnectPhoneItelephony(this)
        }
        relativeAcceptCall.setOnClickListener {
            acceptCall()
        }
    }

    private fun registerCallStateReceiver() {
        callStateReceiver = CallStateReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED)
        registerReceiver(callStateReceiver, intentFilter)
    }

    private fun broadcastHeadsetConnected(connected: Boolean) {
        val i = Intent(Intent.ACTION_HEADSET_PLUG)
        i.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY)
        i.putExtra("state", if (connected) 1 else 0)
        i.putExtra("name", "mysms")
        try {
            sendOrderedBroadcast(i, null)
        } catch (e: Exception) {
        }
    }

    inner class CallStateReceiver : BroadcastReceiver() {
        override fun onReceive(
            context: Context,
            intent: Intent
        ) {
            flashlightProvider.cancelFlash()
            flashlightProvider.turnFlashlightOff()
            finish()
        }
    }

    private fun acceptCall() {
        val broadcastConnected =
            (MANUFACTURER_HTC == (Build.MANUFACTURER)
                    && !audioManager.isWiredHeadsetOn)
        if (broadcastConnected) {
            broadcastHeadsetConnected(false)
        }
        try {
            try {
                Runtime.getRuntime().exec(
                    "input keyevent " +
                            KeyEvent.KEYCODE_HEADSETHOOK.toString()
                )
            } catch (e: IOException) {
                val enforcedPerm = "android.permission.CALL_PRIVILEGED"
                val btnDown: Intent = Intent(Intent.ACTION_MEDIA_BUTTON).putExtra(
                    Intent.EXTRA_KEY_EVENT, KeyEvent(
                        KeyEvent.ACTION_DOWN,
                        KeyEvent.KEYCODE_HEADSETHOOK
                    )
                )
                val btnUp: Intent = Intent(Intent.ACTION_MEDIA_BUTTON).putExtra(
                    Intent.EXTRA_KEY_EVENT, KeyEvent(
                        KeyEvent.ACTION_UP,
                        KeyEvent.KEYCODE_HEADSETHOOK
                    )
                )
                sendOrderedBroadcast(btnDown, enforcedPerm)
                sendOrderedBroadcast(btnUp, enforcedPerm)
            }
        } finally {
            if (broadcastConnected) {
                broadcastHeadsetConnected(false)
            }
        }
    }

    override fun onBackPressed() {
        if (isPreview) {
            setResult(Activity.RESULT_CANCELED)
            finish()
        } else {
            return
        }
        super.onBackPressed()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 6006) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                textViewApply.text = "Waiting..."
                DownloadFileFromURL(
                    name,
                    textViewApply
                ).execute("https://i.imgur.com/n3iX11q.gif")
            }
        }else if(requestCode == 7007){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
                sharedPreferences.edit().putString(Constrains.nameGifSelected, name).apply()
                textViewApply.text = "Applied Successfully"
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }

    private fun listenerApplyUi() {
        relativeBottom.setOnClickListener {
            if (textViewApply.text == "Download") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED ||
                        checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    ) {
                        textViewApply.text = "Waiting..."
                        DownloadFileFromURL(
                            name,
                            textViewApply
                        ).execute("https://i.imgur.com/n3iX11q.gif")
                    } else {
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ), 6006
                        )
                    }
                }
            } else if (textViewApply.text == "Apply it") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED ||
                        checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    ) {
                        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
                        sharedPreferences.edit().putString(Constrains.nameGifSelected, name).apply()
                        textViewApply.text = "Applied Successfully"
                        setResult(Activity.RESULT_OK)
                        finish()
                    } else {
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ), 7007
                        )
                    }
                }
            }
        }
    }

    private fun setDataPhone() {
        val numberPhone = intent.getStringExtra("numberPhone") ?: "Unknown"
        textViewNumberPhone.text = numberPhone
        textViewNameContact.text = getContactName(numberPhone, this)
        val bitmapPhoto = retrieveContactPhoto(this, numberPhone)
        if (bitmapPhoto != null) {
            circleImageView.setImageBitmap(bitmapPhoto)
        } else {
            circleImageView.setColorFilter(resources.getColor(R.color.colorBlack))
            circleImageView.setImageResource(R.drawable.vector_user)
        }
        relativeBottom.visibility = View.GONE

        val nameGif = sharedPreferences.getString(Constrains.nameGifSelected, "")
        val file = File(
            "${Environment.getExternalStorageDirectory().path}${File.separator}${nameGif}.gif"
        )
        if (file.exists()) {
            Glide.with(this).load(file)
                .placeholder(ColorDrawable(Color.parseColor("#9ACCCD")))
                .error(ColorDrawable(Color.parseColor("#9ACCCD")))
                .into(imageViewBackground)
        }
    }

    private fun retrieveContactPhoto(
        context: Context,
        number: String?
    ): Bitmap? {
        var photo = BitmapFactory.decodeResource(
            context.resources,
            R.drawable.vector_user
        )
        try {
            val contentResolver = context.contentResolver
            var contactId = ""
            val uri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number)
            )
            val projection = arrayOf(
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID
            )
            val cursor = contentResolver.query(
                uri,
                projection,
                null,
                null,
                null
            )
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    contactId =
                        cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID))
                }
                cursor.close()
            }

            val inputStream: InputStream? =
                ContactsContract.Contacts.openContactPhotoInputStream(
                    context.contentResolver,
                    ContentUris.withAppendedId(
                        ContactsContract.Contacts.CONTENT_URI,
                        contactId.toLong()
                    )
                )
            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream)
            }
            assert(inputStream != null)
            inputStream?.close()
        } catch (e: IOException) {
            photo = null
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            photo = null
            e.printStackTrace()
        } catch (e: Exception) {
            photo = null
            e.printStackTrace()
        }
        return photo
    }

    private fun getContactName(phoneNumber: String?, context: Context): String? {
        var contactName = "Unknown"
        try {
            val uri: Uri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber)
            )
            val projection =
                arrayOf(ContactsContract.PhoneLookup.DISPLAY_NAME)
            val cursor = context.contentResolver.query(uri, projection, null, null, null)
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    contactName = cursor.getString(0)
                }
                cursor.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return contactName
    }

    private fun checkFileExist(name: String): Boolean {
        val file =
            File(Environment.getExternalStorageDirectory().path + File.separator + name + ".gif")
        return file.exists()
    }

    private fun initData() {
        val avatar = intent.getIntExtra("avatar", R.drawable.ic_avatar_default)
        gifUrl = intent.getStringExtra("gifUrl") ?: ""
        val isSelected = intent.getBooleanExtra("isSelected", true)
        name = intent.getStringExtra("nameTheme") ?: ""

        when {
            isSelected -> {
                textViewApply.text = "Applied Successfully"
            }
            checkFileExist(name ?: "") -> {
                textViewApply.text = "Apply it"
            }
            else -> textViewApply.text = "Download"
        }

        circleImageView.setImageResource(avatar)
        Glide.with(this).load(gifUrl)
            .placeholder(ColorDrawable(Color.parseColor("#9ACCCD")))
            .error(ColorDrawable(Color.parseColor("#9ACCCD")))
            .into(imageViewBackground)
    }

    private fun startAnimation() {
        imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {}

            override fun onAnimationEnd(animation: Animator?) {
                imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {}

                    override fun onAnimationEnd(animation: Animator?) {
                        bubbleAnimation()
                    }

                    override fun onAnimationCancel(animation: Animator?) {}

                    override fun onAnimationStart(animation: Animator?) {}

                }).translationY(0f).setDuration(200).start()
            }

            override fun onAnimationCancel(animation: Animator?) {}

            override fun onAnimationStart(animation: Animator?) {}

        }).translationY(-200f).setDuration(500).start()
    }

    private fun bubbleAnimation() {
        imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {}

            override fun onAnimationEnd(animation: Animator?) {
                imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {}

                    override fun onAnimationEnd(animation: Animator?) {
                        imageViewAnswerCall.animate()
                            .setListener(object : Animator.AnimatorListener {
                                override fun onAnimationRepeat(animation: Animator?) {}

                                override fun onAnimationEnd(animation: Animator?) {
                                    imageViewAnswerCall.animate()
                                        .setListener(object : Animator.AnimatorListener {
                                            override fun onAnimationRepeat(animation: Animator?) {}

                                            override fun onAnimationEnd(animation: Animator?) {
                                                startAnimation()
                                            }

                                            override fun onAnimationCancel(animation: Animator?) {}

                                            override fun onAnimationStart(animation: Animator?) {}

                                        }).translationY(0f).setDuration(200).start()
                                }

                                override fun onAnimationCancel(animation: Animator?) {}

                                override fun onAnimationStart(animation: Animator?) {}

                            }).translationY(-50f).setDuration(200).start()
                    }

                    override fun onAnimationCancel(animation: Animator?) {}

                    override fun onAnimationStart(animation: Animator?) {}

                }).translationY(0f).setDuration(200).start()
            }

            override fun onAnimationCancel(animation: Animator?) {}

            override fun onAnimationStart(animation: Animator?) {}

        }).translationY(-50f).setDuration(200).start()
    }
}
