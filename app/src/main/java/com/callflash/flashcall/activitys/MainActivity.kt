package com.callflash.flashcall.activitys

import android.os.Bundle
import com.callflash.flashcall.commons.MyAppActivity
import com.callflash.flashcall.features.flash.FlashLightFragment
import com.callflash.flashcall.features.settings.SettingsFragment
import com.callflash.flashcall.features.uiCall.UiCallPhoneFragment
import com.callflash.flashcall.R
import com.callflash.flashcall.adapters.MyPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : MyAppActivity() {

    private val iconTabLayout =
        arrayListOf(
            R.drawable.ic_phone,
            R.drawable.ic_torch,
            R.drawable.ic_gear
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViewPager()
    }

    private fun initViewPager() {
        val adapter = MyPagerAdapter(supportFragmentManager)
        adapter.addFragment(UiCallPhoneFragment())
        adapter.addFragment(FlashLightFragment())
        adapter.addFragment(SettingsFragment())
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
        for (i in 0 until tabLayout.tabCount) {
            tabLayout.getTabAt(i)!!.setIcon(iconTabLayout[i])
        }
    }
}
