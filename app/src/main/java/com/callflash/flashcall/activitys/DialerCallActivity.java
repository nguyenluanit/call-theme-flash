package com.callflash.flashcall.activitys;

import android.Manifest;
import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.os.Trace;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.telecom.Call;
import android.text.TextUtils;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.android.phone.common.dialpad.DialpadKeyButton;
import com.android.phone.common.dialpad.DialpadView;
import com.bumptech.glide.Glide;
import com.callflash.flashcall.Constrains;
import com.callflash.flashcall.FlashlightProvider;
import com.callflash.flashcall.R;
import com.callflash.flashcall.commons.dialpad.UnicodeDialerKeyListener;
import com.callflash.flashcall.utils.Animation;
import com.callflash.flashcall.utils.Constants;
import com.callflash.flashcall.utils.OngoingCall;

import java.io.File;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;


import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class DialerCallActivity extends AppCompatActivity implements DialpadKeyButton.OnPressedListener {

    private static DialerCallActivity instance;
    private final int PRIORITY_RECEIVE = 2;
    private final int DIAL_TONE_STREAM_TYPE = AudioManager.STREAM_DTMF;
    private final int TONE_RELATIVE_VOLUME = 80;
    private final HashSet<View> mPressedDialpadKeys = new HashSet<View>(12);

    private LinearLayout mImgHangupOutgoing;
    private ConstraintLayout lnCalling;
    private String mPhoneNumberDisplay;
    private TextView tvPhoneNumber;
    private ImageView imgAvatar;
    private CircleImageView imageContact;
    private ConstraintLayout bgMain;

    private ImageView mImgSpeaker, mImgMicro, mImgHold, mImgKeypad;
    private TextView mTvCallState, mTvPhoneNumber, mTvTimeCallCreate, textViewNameContact;
    private ImageView imageViewBackground, imageViewBackgroundIncoming, imageViewAnswerCall;

    private CompositeDisposable mDisposable;
    private String mPhoneNumber = null;
    private OngoingCall mOngoingCall;
    private AudioManager mAudioManager;
    private boolean isSpeaker, isMicro, isHold;
    private NotificationManager notifyManager = null;
    private Call mCall;
    private boolean isBroastcast;
    private int count = 1;
    private Timer timer = null;
    private boolean mDTMFToneEnabled;
    private ConstraintLayout layoutKeypad, layoutOption;
    public static Context mContext;
    private DialpadView mDialpadView;
    private static final int TONE_LENGTH_INFINITE = -1;
    private EditText mDigits;
    private boolean isCheckVisibleViewTime = false;
    private ToneGenerator mToneGenerator;
    private final Object mToneGeneratorLock = new Object();
    public static String mState = null;
    public static String mStartAndEndCall = null;
    public static String mStateRing = null;
    public static int isAnswered = 1;
    private boolean isIn;
    private BroadcastReceiver mBroadcastReceiver;

    private FlashlightProvider flashlightProvider;
    private SharedPreferences sharedPreferences;


    public static DialerCallActivity getInstance() {
        if (instance == null) instance = new DialerCallActivity();
        return instance;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialer_activity_call);
        dialer_registerBroadcastReceive(this);
        dialer_initView();
        flashlightProvider = new FlashlightProvider(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String nameGif = sharedPreferences.getString(Constrains.nameGifSelected, "");
        File file = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + nameGif + ".gif");
        if (file.exists()) {
            Glide.with(this).load(file)
                    .placeholder(new ColorDrawable(getResources().getColor(R.color.bg_calling)))
                    .error(new ColorDrawable(getResources().getColor(R.color.bg_calling)))
                    .into(imageViewBackground);
            Glide.with(this).load(file)
                    .placeholder(new ColorDrawable(getResources().getColor(R.color.bg_calling)))
                    .error(new ColorDrawable(getResources().getColor(R.color.bg_calling)))
                    .into(imageViewBackgroundIncoming);
        }

        instance = this;
        mAudioManager = (AudioManager) getApplicationContext().getSystemService(AUDIO_SERVICE);


        mOngoingCall = new OngoingCall();
        mDisposable = new CompositeDisposable();
        if (timer != null) {
            timer.cancel();
        }

        timer = new Timer();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);


        if (getIntent() != null && getIntent().getData() != null && getIntent().getData().getSchemeSpecificPart() != null) {
            mPhoneNumber = getIntent().getData().getSchemeSpecificPart();
            mPhoneNumberDisplay = dialer_getContactName(mPhoneNumber);
            if (TextUtils.isEmpty(mPhoneNumberDisplay)) {
                mPhoneNumberDisplay = mPhoneNumber;
            }
        } else {
            mPhoneNumberDisplay = "Unknown contact";
        }

        View oneButton = findViewById(R.id.one);
        if (oneButton != null) {
            dialer_configureKeypadListeners(layoutKeypad);
        }

        isIn = !getIntent().getBooleanExtra("isOut", false);
        dialer_setInComingView();
        dialer_onClick();


    }

    @Override
    protected void onDestroy() {
        flashlightProvider.cancelFlash();
        flashlightProvider.turnFlashlightOff();
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void dialer_enterReveal() {
        bgMain.post(() -> {
            int cx = bgMain.getMeasuredWidth() / 2;
            int cy = bgMain.getMeasuredHeight() / 2;
            int finalRadius = Math.max(bgMain.getWidth(), bgMain.getHeight()) / 2;
            Animator anim = ViewAnimationUtils.createCircularReveal(bgMain, cx, cy, 0, finalRadius);
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    imageContact.setVisibility(View.VISIBLE);
                    mTvPhoneNumber.setVisibility(View.VISIBLE);
                    mTvCallState.setVisibility(View.VISIBLE);
                    layoutOption.setVisibility(View.VISIBLE);
                    mImgHangupOutgoing.setVisibility(View.VISIBLE);
                    layoutOption.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.slide_up));
                    mImgHangupOutgoing.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.slide_up));
                    mTvPhoneNumber.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.slide_up));
                    mTvCallState.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.slide_up));
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            anim.start();
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void dialer_setInComingView() {
        Bitmap bitmap = dialer_getPhoto(mPhoneNumber);
        if (bitmap != null) {
            imgAvatar.setImageBitmap(bitmap);
        } else imgAvatar.setImageResource(R.drawable.vector_user_black);
        dialer_enterReveal();
        if (!getIntent().getBooleanExtra("isOut", false)) {
            boolean isTurnFlash = sharedPreferences.getBoolean(Constrains.isTurnOnLedFlash, false);

            if (isTurnFlash) {
                int countFlash = sharedPreferences.getInt(Constrains.COUNT_FLASH, 5);
                flashlightProvider.startThread();
                flashlightProvider.toggleFlash(10, countFlash, 100, ((countFlash % 10) * 100) * 3);
            }
            textViewNameContact.setText(getContactName(mPhoneNumber, this));
            lnCalling.setVisibility(View.VISIBLE);
            tvPhoneNumber.setText(mPhoneNumber);

            Animation.Companion.startAnimation(imageViewAnswerCall);
        } else {
//            bgMain.setBackgroundColor(getResources().getColor(R.color.bg_calling));
//            if (bitmap != null) {
//                imageContact.setImageBitmap(bitmap);
//            }
        }
    }

    private String getContactName(String phoneNumber, Context context) {
        String contactName = "Unknown";
        try {
            Uri uri = Uri.withAppendedPath(
                    ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                    Uri.encode(phoneNumber)
            );
            String[] projection =
                    new String[]{(ContactsContract.PhoneLookup.DISPLAY_NAME)};
            Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    contactName = cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contactName;
    }


    private void dialer_hideInComingView() {
        if (lnCalling != null)
            lnCalling.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPressedDialpadKeys.clear();
        mDTMFToneEnabled = Settings.System.getInt(getContentResolver(),
                Settings.System.DTMF_TONE_WHEN_DIALING, 1) == 1;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPressedDialpadKeys.clear();
        if (mBroadcastReceiver != null) {
            unregisterReceiver(mBroadcastReceiver);
            mBroadcastReceiver = null;
        }
    }

    private void dialer_onClick() {
        mImgHangupOutgoing.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                dialer_onHangupClicked();
            }
        });
        mImgSpeaker.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (isSpeaker) {
                    isSpeaker = false;
                    mAudioManager.setSpeakerphoneOn(false);
                    mImgSpeaker.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorWhite)));
                } else {
                    isSpeaker = true;
                    mAudioManager.setSpeakerphoneOn(true);
                    mImgSpeaker.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.tool_theme)));
                }
            }
        });

        mImgMicro.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if (isMicro) {
                    isMicro = false;
                    mAudioManager.setMicrophoneMute(false);
                    mImgMicro.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorWhite)));
                } else {
                    isMicro = true;
                    mAudioManager.setMicrophoneMute(true);
                    mImgMicro.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.tool_theme)));
                }
            }
        });

        mImgKeypad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCheckVisibleViewTime = true;
                layoutOption.setVisibility(View.GONE);
                imageContact.setVisibility(View.GONE);
                mImgHangupOutgoing.setVisibility(View.GONE);
                mTvPhoneNumber.setVisibility(View.GONE);
                mTvTimeCallCreate.setVisibility(View.GONE);
                mTvCallState.setVisibility(View.GONE);
                layoutKeypad.setVisibility(View.VISIBLE);
                layoutKeypad.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide_up));
            }
        });

        mImgHold.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (isHold) {
                    isHold = false;
                    mOngoingCall.truecall_phone_hold(false);
                    mImgHold.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorWhite)));
                } else {
                    isHold = true;
                    mOngoingCall.truecall_phone_hold(true);
                    mImgHold.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.tool_theme)));
                }
            }
        });
        findViewById(R.id.dialpad_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCheckVisibleViewTime = false;
                layoutKeypad.setVisibility(View.GONE);
                layoutKeypad.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide_down));
                layoutOption.setVisibility(View.VISIBLE);
                mTvPhoneNumber.setVisibility(View.VISIBLE);
                mTvTimeCallCreate.setVisibility(View.VISIBLE);
                mTvCallState.setVisibility(View.VISIBLE);
                imageContact.setVisibility(View.VISIBLE);
                mImgHangupOutgoing.setVisibility(View.VISIBLE);
            }
        });
    }

    private void dialer_configureKeypadListeners(View fragmentView) {
        final int[] buttonIds = new int[]{R.id.one, R.id.two, R.id.three, R.id.four, R.id.five,
                R.id.six, R.id.seven, R.id.eight, R.id.nine, R.id.star, R.id.zero, R.id.pound};

        DialpadKeyButton dialpadKey;

        for (int buttonId : buttonIds) {
            dialpadKey = fragmentView.findViewById(buttonId);
            dialpadKey.setOnPressedListener(this);
        }
    }

    private void dialer_initView() {
        imageViewAnswerCall = findViewById(R.id.imageViewAnswerCall);
        textViewNameContact = findViewById(R.id.textViewNameContact);
        imageViewBackgroundIncoming = findViewById(R.id.imageViewBackgroundIncoming);
        imageViewBackground = findViewById(R.id.imageViewBackground);
        bgMain = findViewById(R.id.bg_main);
        imageContact = findViewById(R.id.img_contact);
        mTvPhoneNumber = findViewById(R.id.tv_phone_number);
        lnCalling = findViewById(R.id.lnCalling);
        mTvCallState = findViewById(R.id.callInfo);
        mTvTimeCallCreate = findViewById(R.id.tv_time_call_create);
        layoutOption = findViewById(R.id.layout_option);
        layoutKeypad = findViewById(R.id.layout_keypad);
        mImgSpeaker = findViewById(R.id.img_speaker);
        mImgMicro = findViewById(R.id.img_micro);
        mImgHold = findViewById(R.id.img_hold);
        mImgKeypad = findViewById(R.id.img_keypad);

        tvPhoneNumber = findViewById(R.id.tvNumber);
        imgAvatar = findViewById(R.id.imgAvatar);

        mImgHangupOutgoing = findViewById(R.id.btn_hangup_outgoing);

        mDialpadView = findViewById(R.id.dialpad_view);
        mDialpadView.setCanDigitsBeEdited(true);
        mDigits = mDialpadView.getDigits();
        mDigits.setKeyListener(UnicodeDialerKeyListener.INSTANCE);
        findViewById(R.id.deleteButton).setVisibility(View.INVISIBLE);
        findViewById(R.id.dialpad_overflow).setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void dialer_onAnswerClicked() {
        mOngoingCall.truecall_phone_answer();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void dialer_onHangupClicked() {
        mOngoingCall.truecall_phone_hangup();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @SuppressLint("AutoDispose")
    @Override
    protected void onStart() {
        super.onStart();
        assert updateUi(-1) != null;
        mDisposable.add(
                OngoingCall.state
                        .subscribe(integer -> updateUi(integer)));

        mDisposable.add(
                OngoingCall.state
                        .filter(integer -> integer == Call.STATE_DISCONNECTED)
                        .delay(1, TimeUnit.SECONDS)
                        .firstElement()
                        .subscribe(integer -> finishAndRemoveTask()));

        final long start = System.currentTimeMillis();
        synchronized (mToneGeneratorLock) {
            if (mToneGenerator == null) {
                try {
                    mToneGenerator = new ToneGenerator(DIAL_TONE_STREAM_TYPE, TONE_RELATIVE_VOLUME);
                } catch (RuntimeException e) {
                    mToneGenerator = null;
                }
            }
        }
        final long total = System.currentTimeMillis() - start;
        if (total > 50) {
        }
        Trace.endSection();
    }

    @SuppressLint("SetTextI18n")
    private Consumer<? super Integer> updateUi(Integer state) {
        mTvCallState.setText("" + Constants.asString(state));

        mTvPhoneNumber.setText("" + mPhoneNumber);

        try {
            if (state == Call.STATE_ACTIVE) {
//                bgMain.setBackgroundColor(getResources().getColor(R.color.bg_call_active));
                if (!isCheckVisibleViewTime) {
                    mTvTimeCallCreate.setVisibility(View.VISIBLE);
                }
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mTvTimeCallCreate.setText("" + String.format("%02d:%02d",
                                        (count % 3600) / 60, (count % 60)));
                                count++;
                            }
                        });
                    }
                }, 1000, 1000);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onBackPressed() {
        flashlightProvider.cancelFlash();
    }

    @Override
    protected void onStop() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        super.onStop();
        mDisposable.clear();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void start(Context context, Call call) {
        if (call.getState() == Call.STATE_RINGING) {
            isBroastcast = true;
            mStateRing = String.valueOf(call.getState());
            mStartAndEndCall = String.valueOf(call.getState());
            @SuppressLint({"NewApi", "LocalSuppress"})
            Intent intent = new Intent(context, DialerCallActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .setData(call.getDetails().getHandle());
            intent.putExtra("isOut", false);
            context.startActivity(intent);
        } else if (call.getState() == Call.STATE_DISCONNECTED) {
            if (notifyManager != null)
                notifyManager.cancelAll();
            dialer_hideInComingView();
            mState = String.valueOf(call.getState());
        } else {
            @SuppressLint({"NewApi", "LocalSuppress"})
            Intent intent = new Intent(context, DialerCallActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .setData(call.getDetails().getHandle());
            intent.putExtra("isOut", true);
            context.startActivity(intent);
            isBroastcast = false;
            mStartAndEndCall = String.valueOf(call.getState());
            mStateRing = String.valueOf(call.getState());
        }
    }


    @SuppressLint("NewApi")
    private void dialer_sendNotification(Context context, Call call) {
        mCall = call;
        int NOTIFY_ID = 1002;

        Intent decline = new Intent(Constants.ACTION_DECLINE);
        Intent answer = new Intent(Constants.ACTION_ANSWER);

        String name = "KotlinApplication";
        String id = "kotlin_app";
        String description = "kotlin_app_first_channel";

        Intent intent;
        PendingIntent pendingIntent;
        NotificationCompat.Builder builder;

        if (notifyManager == null) {
            notifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notifyManager.getNotificationChannel(id);
            if (mChannel == null) {
                mChannel = new NotificationChannel(id, name, importance);
                mChannel.setDescription(description);
                mChannel.setShowBadge(true);
                mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                mChannel.enableLights(true);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{300, 300, 300});
                mChannel.setLightColor(Color.GREEN);
                notifyManager.createNotificationChannel(mChannel);
            }
        }

        builder = new NotificationCompat.Builder(context, id);

        intent = new Intent();
        pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        builder.setContentTitle(call.getDetails().getHandle().getSchemeSpecificPart())
                .setDefaults(Notification.DEFAULT_ALL)
                .setCategory(Notification.CATEGORY_CALL)
                .setOngoing(true)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_speaker)
                .setTicker("Hearty365")
                .setPriority(Notification.PRIORITY_MAX)
                .setContentText("Incoming call_call")
                .setFullScreenIntent(pendingIntent, true)
                .setVibrate(new long[]{0, 1000, 500, 1000})
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE))
                .setContentInfo("Info");

        PendingIntent pendingDismissIntent = PendingIntent.getBroadcast(context, 0, decline, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Action dismissAction = new NotificationCompat.Action(R.drawable.ic_speaker,
                "Decline", pendingDismissIntent);
        builder.addAction(dismissAction);

        Intent answerIntent = new Intent(context, DialerCallActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setData(call.getDetails().getHandle());


        answerIntent.setAction("DISMISS");
        PendingIntent pendingDismissIntentt = PendingIntent.getBroadcast(context, 0, answer, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Action dismissActionn = new NotificationCompat.Action(R.drawable.ic_speaker,
                "Answer", pendingDismissIntentt);
        builder.addAction(dismissActionn);
//        builder.phone_setColor(ContextCompat.phone_getColor(context, R.color.colorPrimary));

        Notification notification = builder.build();
        notifyManager.notify(NOTIFY_ID, notification);
    }

    private BroadcastReceiver dialer_getBroadcast() {
        return new BroadcastReceiver() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction() != null) {
                    OngoingCall ongoingCall = new OngoingCall();
                    switch (intent.getAction()) {
                        case Constants.ACTION_DECLINE:
                            ongoingCall.truecall_phone_hangup();
                            break;
                        case Constants.ACTION_ANSWER:
                            ongoingCall.truecall_phone_answer();
                            if (notifyManager != null) {
                                notifyManager.cancelAll();
                            }
                            isAnswered = 2;
                            @SuppressLint({"NewApi", "LocalSuppress"})
                            Intent answerIntent = new Intent(context, DialerCallActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    .setData(mCall.getDetails().getHandle());
                            context.startActivity(answerIntent);


                            break;
                    }
                }
            }
        };
    }

    private void dialer_registerBroadcastReceive(Context context) {
        if (mBroadcastReceiver == null) {
            mBroadcastReceiver = dialer_getBroadcast();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_DECLINE);
            intentFilter.addAction(Constants.ACTION_ANSWER);
            intentFilter.setPriority(PRIORITY_RECEIVE);
            context.registerReceiver(mBroadcastReceiver, intentFilter);
        }
    }

    private void dialer_keyPressed(int keyCode) {
        if (layoutKeypad == null || layoutKeypad.getTranslationY() != 0) {
            return;
        }
        switch (keyCode) {
            case KeyEvent.KEYCODE_1:
                dialer_playTone(ToneGenerator.TONE_DTMF_1, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_2:
                dialer_playTone(ToneGenerator.TONE_DTMF_2, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_3:
                dialer_playTone(ToneGenerator.TONE_DTMF_3, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_4:
                dialer_playTone(ToneGenerator.TONE_DTMF_4, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_5:
                dialer_playTone(ToneGenerator.TONE_DTMF_5, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_6:
                dialer_playTone(ToneGenerator.TONE_DTMF_6, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_7:
                dialer_playTone(ToneGenerator.TONE_DTMF_7, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_8:
                dialer_playTone(ToneGenerator.TONE_DTMF_8, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_9:
                dialer_playTone(ToneGenerator.TONE_DTMF_9, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_0:
                dialer_playTone(ToneGenerator.TONE_DTMF_0, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_POUND:
                dialer_playTone(ToneGenerator.TONE_DTMF_P, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_STAR:
                dialer_playTone(ToneGenerator.TONE_DTMF_S, TONE_LENGTH_INFINITE);
                break;
            default:
                break;
        }

        layoutKeypad.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
        mDigits.onKeyDown(keyCode, event);

        // If the cursor is at the phone_end of the text we hide it.
        final int length = mDigits.length();
        if (length == mDigits.getSelectionStart() && length == mDigits.getSelectionEnd()) {
            mDigits.setCursorVisible(false);
        }
    }

    private void dialer_playTone(int tone, int durationMs) {
        // if local tone playback is disabled, just return.
        if (!mDTMFToneEnabled) {
            return;
        }

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mOngoingCall.getsCall().playDtmfTone(getChar(tone));
                mOngoingCall.getsCall().stopDtmfTone();
            } else
                Toast.makeText(instance, "This device is not supported", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(instance, "This device is not supported", Toast.LENGTH_SHORT).show();
        }

        // Also do nothing if the phone is in silent mode.
        // We need to re-isAnswered the ringer mode for *every* dialer_playTone()
        // call_call, rather than keeping a local flag that's updated in
        // dialer_onResume(), since it's possible to toggle silent mode without
        // leaving the current activity (via the ENDCALL-longpress menu.)
        AudioManager audioManager =
                (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        int ringerMode = audioManager.getRingerMode();
        if ((ringerMode == AudioManager.RINGER_MODE_SILENT)
                || (ringerMode == AudioManager.RINGER_MODE_VIBRATE)) {
            return;
        }

        synchronized (mToneGeneratorLock) {
            if (mToneGenerator == null) {
//                truecall_phone_Log.w(TAG, "dialer_playTone: mToneGenerator == null, tone: " + tone);
                return;
            }

            // Start the new tone (will phone_stop any playing tone)
            mToneGenerator.startTone(tone, durationMs);
        }
    }

    private char getChar(int tone) {
        if (tone == 0) return '0';
        else if (tone == 1) return '1';
        else if (tone == 2) return '2';
        else if (tone == 3) return '3';
        else if (tone == 4) return '4';
        else if (tone == 5) return '5';
        else if (tone == 6) return '6';
        else if (tone == 7) return '7';
        else if (tone == 8) return '8';
        else if (tone == 9) return '9';
        else if (tone == 10) return '*';
        else return '#';
    }

    /**
     * Stop the tone if it is played.
     */
    private void dialer_stopTone() {
        // if local tone playback is disabled, just return.
        if (!mDTMFToneEnabled) {
            return;
        }
        synchronized (mToneGeneratorLock) {
            if (mToneGenerator == null) {
//                truecall_phone_Log.w(TAG, "dialer_stopTone: mToneGenerator == null");
                return;
            }
            mToneGenerator.stopTone();
        }
    }

    @Override
    public void onPressed(View view, boolean pressed) {
//        if (DEBUG) truecall_phone_Log.d(TAG, "onPressed(). view: " + view + ", pressed: " + pressed);
        if (pressed) {
            int resId = view.getId();
            if (resId == R.id.one) {
                dialer_keyPressed(KeyEvent.KEYCODE_1);
            } else if (resId == R.id.two) {
                dialer_keyPressed(KeyEvent.KEYCODE_2);
            } else if (resId == R.id.three) {
                dialer_keyPressed(KeyEvent.KEYCODE_3);
            } else if (resId == R.id.four) {
                dialer_keyPressed(KeyEvent.KEYCODE_4);
            } else if (resId == R.id.five) {
                dialer_keyPressed(KeyEvent.KEYCODE_5);
            } else if (resId == R.id.six) {
                dialer_keyPressed(KeyEvent.KEYCODE_6);
            } else if (resId == R.id.seven) {
                dialer_keyPressed(KeyEvent.KEYCODE_7);
            } else if (resId == R.id.eight) {
                dialer_keyPressed(KeyEvent.KEYCODE_8);
            } else if (resId == R.id.nine) {
                dialer_keyPressed(KeyEvent.KEYCODE_9);
            } else if (resId == R.id.zero) {
                dialer_keyPressed(KeyEvent.KEYCODE_0);
            } else if (resId == R.id.pound) {
                dialer_keyPressed(KeyEvent.KEYCODE_POUND);
            } else if (resId == R.id.star) {
                dialer_keyPressed(KeyEvent.KEYCODE_STAR);
            } else {
//                truecall_phone_Log.truecall_phone_wtf(TAG, "Unexpected onTouch(ACTION_DOWN) event from: " + view);
            }
            mPressedDialpadKeys.add(view);
        } else {
            mPressedDialpadKeys.remove(view);
            if (mPressedDialpadKeys.isEmpty()) {
                dialer_stopTone();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void dialer_declineCall(View view) {
//        truecall_phone_OngoingCall.getsCall().disconnect();
        dialer_onHangupClicked();
        finishAndRemoveTask();

        flashlightProvider.cancelFlash();
        flashlightProvider.turnFlashlightOff();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void dialer_acceptCall(View view) {
//        truecall_phone_OngoingCall.getsCall().truecall_phone_answer(VideoProfile.STATE_AUDIO_ONLY);
        dialer_onAnswerClicked();
        isAnswered = 2;
        dialer_hideInComingView();

        flashlightProvider.cancelFlash();
    }


    private String dialer_getContactName(String phoneNumber) {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
            String projection[] = {ContactsContract.PhoneLookup.DISPLAY_NAME};
            String contactName = "";
            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    contactName = cursor.getString(0);
                }
                cursor.close();
            }
            return contactName;
        } else {
            return "";
        }
    }

    private Bitmap dialer_getPhoto(String phoneNumber) {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            Uri phoneUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
            ContentResolver cr = getContentResolver();
            Cursor contact = cr.query(phoneUri, new String[]{ContactsContract.Contacts._ID}, null, null, null);
            if (contact != null && contact.moveToFirst()) {
                long userId = contact.getLong(
                        contact.getColumnIndex(
                                ContactsContract.Contacts._ID
                        )
                );
                Uri photoUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, userId);
                InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, photoUri);
                contact.close();
                if (input != null) {
                    return BitmapFactory.decodeStream(input);
                }
            }
            return BitmapFactory.decodeResource(getResources(), R.drawable.vector_user_black);
        } else {
            return null;
        }
    }

    //add junk code
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_background_color)));
        }
    }

    private Bitmap getBitmapFromDrawable(Drawable drawable) {
        if (drawable == null) {
            return null;
        }

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        try {
            Bitmap bitmap = null;

            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                        drawable.getIntrinsicWidth(), Bitmap.Config.ALPHA_8);

            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                        drawable.getIntrinsicHeight(), Bitmap.Config.ALPHA_8);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

}
