package com.callflash.flashcall.models

data class Theme(val name: String, val thumbUrl: String, val gifUrl: String)