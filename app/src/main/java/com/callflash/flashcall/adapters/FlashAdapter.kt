package com.callflash.flashcall.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.callflash.flashcall.R

class FlashAdapter(
    private val context: Context
) : RecyclerView.Adapter<FlashAdapter.ViewHolder>() {

    private var currentPosition = 50

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewNumber = itemView.findViewById<TextView>(R.id.textViewNumber)!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.recycler_item_flash, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return 100
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position == currentPosition) {
            holder.textViewNumber.setTextColor(context.resources.getColor(R.color.colorGreen))
        } else holder.textViewNumber.setTextColor(context.resources.getColor(R.color.colorGrayWhite))
        holder.textViewNumber.text = (position % 10).toString()
    }

    fun setCurrentView(position: Int) {
        currentPosition = position
        notifyDataSetChanged()
    }

}