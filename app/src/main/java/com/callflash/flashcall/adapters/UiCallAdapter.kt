package com.callflash.flashcall.adapters

import android.Manifest
import android.animation.Animator
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Environment
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.callflash.flashcall.Constrains
import com.callflash.flashcall.R
import com.callflash.flashcall.activitys.ReceiveIncomingCallActivity
import com.callflash.flashcall.commons.GlideEngine
import com.callflash.flashcall.features.uiCall.UiCallPhoneFragment
import com.callflash.flashcall.models.Theme
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_preview_ui_call.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class UiCallAdapter(
    private val context: Context,
    private val fragment: UiCallPhoneFragment,
    private val listTheme: ArrayList<Theme>
) : RecyclerView.Adapter<UiCallAdapter.ViewHolder>() {

    private val listAvatar = arrayListOf(
        R.drawable.avatar_1,
        R.drawable.avatar_2,
        R.drawable.avatar_3,
        R.drawable.avatar_4,
        R.drawable.avatar_5,
        R.drawable.ic_avatar_default
    )

    private var sharedPreferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    private val listNameContact =
        arrayListOf("Abilene", "Jackson", "Agnes", "Blossom", "Margaret", "Zulema")

    private val vibrantLightColorList = arrayOf(
        ColorDrawable(Color.parseColor("#9ACCCD")), ColorDrawable(Color.parseColor("#8FD8A0")),
        ColorDrawable(Color.parseColor("#CBD890")), ColorDrawable(Color.parseColor("#DACC8F")),
        ColorDrawable(Color.parseColor("#D9A790")), ColorDrawable(Color.parseColor("#D18FD9")),
        ColorDrawable(Color.parseColor("#FF6772")), ColorDrawable(Color.parseColor("#DDFB5C"))
    )

    private fun getRandomDrawableColor(): ColorDrawable? {
        val idx: Int = Random().nextInt(vibrantLightColorList.size)
        return vibrantLightColorList[idx]
    }

    private val glideEngine = GlideEngine()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val circleImageView = itemView.findViewById<CircleImageView>(R.id.circleImageView)!!
        val textViewNameContact = itemView.findViewById<TextView>(R.id.textViewNameContact)!!
        val imageViewBackground =
            itemView.findViewById<ImageView>(R.id.imageViewBackground)!!
        val relativeTick = itemView.findViewById<RelativeLayout>(R.id.relativeTick)!!
        val imageViewAnswerCall = itemView.findViewById<ImageView>(R.id.imageViewAnswerCall)!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.recycler_item_ui_call_phone, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return listTheme.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val colorDrawable = getRandomDrawableColor()
        glideEngine.loadImageUrl(
            context,
            holder.imageViewBackground,
            listTheme[position].thumbUrl,
            colorDrawable
        )
        glideEngine.loadAvatar(context, holder.circleImageView, listAvatar[position])
        holder.textViewNameContact.text = listNameContact[position]
        if (sharedPreferences.getString(
                Constrains.nameGifSelected,
                ""
            ) == listTheme[position].name
        ) {
            holder.relativeTick.visibility = View.VISIBLE

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                ) {
                    val file = File(
                        "${Environment.getExternalStorageDirectory().path}${File.separator}${listTheme[position].name}.gif"
                    )
                    if (file.exists()) {
                        Glide.with(context).load(file)
                            .into(holder.imageViewBackground)
                        startAnimation(holder.imageViewAnswerCall)
                    }
                }
            } else {
                val file = File(
                    "${Environment.getExternalStorageDirectory().path}${File.separator}${listTheme[position].name}.gif"
                )
                if (file.exists()) {
                    Glide.with(context).load(file)
                        .into(holder.imageViewBackground)
                    startAnimation(holder.imageViewAnswerCall)
                }
            }
        } else holder.relativeTick.visibility = View.INVISIBLE
        holder.itemView.setOnClickListener {
            val intent = Intent(context, ReceiveIncomingCallActivity::class.java)
            intent.putExtra("isPreview", true)
            intent.putExtra("isSelected", holder.relativeTick.visibility == View.VISIBLE)
            intent.putExtra("nameTheme", listTheme[position].name)
            intent.putExtra("gifUrl", listTheme[position].gifUrl)
            intent.putExtra("avatar", listAvatar[position])
            fragment.startActivityForResult(intent, Constrains.RESULT_PREVIEW)
        }
    }

    private fun startAnimation(imageViewAnswerCall: ImageView) {
        imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {}

            override fun onAnimationEnd(animation: Animator?) {
                imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {}

                    override fun onAnimationEnd(animation: Animator?) {
                        bubbleAnimation(imageViewAnswerCall)
                    }

                    override fun onAnimationCancel(animation: Animator?) {}

                    override fun onAnimationStart(animation: Animator?) {}

                }).translationY(0f).setDuration(200).start()
            }

            override fun onAnimationCancel(animation: Animator?) {}

            override fun onAnimationStart(animation: Animator?) {}

        }).translationY(-100f).setDuration(500).start()
    }

    private fun bubbleAnimation(imageViewAnswerCall: ImageView) {
        imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {}

            override fun onAnimationEnd(animation: Animator?) {
                imageViewAnswerCall.animate().setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {}

                    override fun onAnimationEnd(animation: Animator?) {
                        imageViewAnswerCall.animate()
                            .setListener(object : Animator.AnimatorListener {
                                override fun onAnimationRepeat(animation: Animator?) {}

                                override fun onAnimationEnd(animation: Animator?) {
                                    imageViewAnswerCall.animate()
                                        .setListener(object : Animator.AnimatorListener {
                                            override fun onAnimationRepeat(animation: Animator?) {}

                                            override fun onAnimationEnd(animation: Animator?) {
                                                startAnimation(imageViewAnswerCall)
                                            }

                                            override fun onAnimationCancel(animation: Animator?) {}

                                            override fun onAnimationStart(animation: Animator?) {}

                                        }).translationY(0f).setDuration(200).start()
                                }

                                override fun onAnimationCancel(animation: Animator?) {}

                                override fun onAnimationStart(animation: Animator?) {}

                            }).translationY(-25f).setDuration(200).start()
                    }

                    override fun onAnimationCancel(animation: Animator?) {}

                    override fun onAnimationStart(animation: Animator?) {}

                }).translationY(0f).setDuration(200).start()
            }

            override fun onAnimationCancel(animation: Animator?) {}

            override fun onAnimationStart(animation: Animator?) {}

        }).translationY(-25f).setDuration(200).start()
    }
}