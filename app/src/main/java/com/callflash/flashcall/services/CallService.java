package com.callflash.flashcall.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.telecom.Call;
import android.telecom.InCallService;

import androidx.annotation.RequiresApi;
import com.callflash.flashcall.activitys.DialerCallActivity;
import com.callflash.flashcall.utils.OngoingCall;

import static android.app.NotificationChannel.DEFAULT_CHANNEL_ID;

@RequiresApi(api = Build.VERSION_CODES.M)
public class CallService extends InCallService {

    @Override
    public void onCallAdded(Call call) {
        super.onCallAdded(call);
        if (call.getState() == Call.STATE_RINGING) {
//            String number = call.getDetails().getHandle().getSchemeSpecificPart();
//
//            if (blockListCallManager.isBlocked(number)
//                    || (manager.isBlockNotInPhonebookEnable() && getContactName(number, this).isEmpty())) {
//                if (manager.isRejectCallEnable()) {
//                    call.disconnect();
//                    if (manager.isShowNotifyCallEnable()) {
//                        showNotifyBlockCall(number);
//                    }
//                    return;
//                } else {
//                    AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//                    if (audioManager != null) {
//                        audioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
//                    }
//                }
//            }
//            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//            if (audioManager != null) {
//                audioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
//            }
        }
        new OngoingCall().truecall_phone_setCall(call);
        DialerCallActivity.getInstance().start(this, call);
    }

//    private void showNotifyBlockCall(String number) {
//        NotificationCompat.Builder notification =
//                new NotificationCompat.Builder(getApplicationContext(), "notifications_default")
//                        .setCategory(NotificationCompat.CATEGORY_CALL)
//                        .setPriority(NotificationCompat.PRIORITY_MAX)
//                        .setSmallIcon(R.drawable.ic_notification)
//                        .setAutoCancel(true)
//                        .setLights(Color.WHITE, 500, 2000)
//                        .setWhen(System.currentTimeMillis())
//                        .setContentTitle("Blocked call")
//                        .setContentText("Blocked call from " + number);
//
//        NotificationManager notificationManager = getSystemService(NotificationManager.class);
//        if (notificationManager != null) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                notificationManager.createNotificationChannel(getNotifyChannel());
//            }
//            notificationManager.notify(0, notification.build());
//        }
//    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private NotificationChannel getNotifyChannel() {
        NotificationChannel notificationChannel = new NotificationChannel(
                DEFAULT_CHANNEL_ID,
                "Default",
                NotificationManager.IMPORTANCE_HIGH
        );
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        return notificationChannel;
    }

    @Override
    public void onCallRemoved(Call call) {
        super.onCallRemoved(call);
//        if (!manager.isRejectCallEnable()) {
//            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//            if (audioManager != null) {
//                audioManager.setStreamVolume(AudioManager.STREAM_RING, 20, 0);
//            }
//        }
        new OngoingCall().truecall_phone_setCall(null);
        DialerCallActivity.getInstance().start(this, call);
    }

    public String getContactName(final String phoneNumber, Context context) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        String contactName = "";
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(0);
            }
            cursor.close();
        }
        return contactName;
    }
}
