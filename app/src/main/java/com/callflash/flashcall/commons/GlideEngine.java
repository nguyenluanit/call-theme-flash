package com.callflash.flashcall.commons;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

import com.callflash.flashcall.R;
import de.hdodenhof.circleimageview.CircleImageView;

@GlideModule
public class GlideEngine extends AppGlideModule {

    public void loadImageUrl(Context context, ImageView imageView, String url, ColorDrawable colorDrawable) {
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .error(colorDrawable)
                        .placeholder(colorDrawable))
                .into(imageView);
    }

    public void loadAvatar(Context context, CircleImageView circleImageView, Integer resId) {
        Glide.with(context)
                .load(resId)
                .apply(new RequestOptions()
                        .error(R.drawable.ic_avatar_default)
                        .placeholder(R.drawable.ic_avatar_default))
                .into(circleImageView);
    }
}
