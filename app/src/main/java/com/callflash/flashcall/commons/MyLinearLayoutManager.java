package com.callflash.flashcall.commons;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;

public class MyLinearLayoutManager extends LinearLayoutManager {

    public MyLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    boolean isCanScrollHorizon = true;

    public void setCanScrollHorizon(boolean canScrollHorizon) {
        isCanScrollHorizon = canScrollHorizon;
    }


    @Override
    public boolean canScrollHorizontally() {
        return isCanScrollHorizon;
    }
}
